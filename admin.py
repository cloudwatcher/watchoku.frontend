from watchoku.rankings.models import Site
from watchoku.rankings.models import Item
from watchoku.rankings.models import Site_User
from watchoku.rankings.models import Bid
from watchoku.forum.models import Topic
from watchoku.forum.models import Board
from watchoku.blog.models import Blog
from watchoku.blog.models import Tag
from watchoku.models import User
from django.contrib import admin

admin.site.register(Site)
admin.site.register(Item)
admin.site.register(Site_User)
admin.site.register(Bid)
admin.site.register(Topic)
admin.site.register(Board)
admin.site.register(Blog)
admin.site.register(Tag)
admin.site.register(User)