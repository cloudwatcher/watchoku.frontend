from django.db import models
class Topic(models.Model):
	board_id = models.IntegerField(max_length = 1000)
	title = models.TextField()
	body = models.TextField()
	depth = models.IntegerField()
	updated = models.DateTimeField(auto_now_add=1000)
	author = models.ForeignKey('watchoku.User')
	is_top = models.BooleanField()

class Board(models.Model):
	name = models.CharField(max_length = 200)
	moderator = models.CommaSeparatedIntegerField(max_length = 1000)
	new_post = models.IntegerField(max_length = 1000)
# Create your models here.
