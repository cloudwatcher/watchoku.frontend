from django.db import models
class Blog(models.Model):
	title = models.TextField()
	teaser = models.TextField()
	body = models.TextField()
	depth = models.IntegerField()
	updated = models.DateTimeField()
	author = models.ForeignKey('watchoku.User')
	tags = models.CommaSeparatedIntegerField(max_length =50)

class Tag(models.Model):
	name = models.CharField(max_length = 100)
	blog_ids = models.CommaSeparatedIntegerField(max_length = 5000)
	blog_numbers = models.IntegerField()
# Create your models here.