from django.template import Context, loader
from django.http import HttpResponse

from watchoku.blog.models import Blog,Tag

def index(request):
	
	latest_blog_list = Blog.objects.all().order_by('updated')[:10]
	tag_list = Tag.objects.all()
	
	t = loader.get_template('default/html/index.html')
	
	c = Context({
		'latest_blog_list': latest_blog_list,
		})
	
	return HttpResponse(t.render(c))