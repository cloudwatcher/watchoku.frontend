from django.db import models

class Site(models.Model):
	url = models.URLField(verify_exists=True,max_length = 100)
class Item(models.Model):
	itemid = models.IntegerField()
	site = models.ForeignKey('Site')
	title = models.CharField(max_length = 40)
class Site_User(models.Model):
	site = models.ForeignKey('Site')
	username = models.CharField(max_length = 100)
	spent = models.IntegerField()
	bidcount = models.IntegerField()
	isrobot	= models.BooleanField()
class Bid(models.Model):
	item = models.ForeignKey('Item')
	user = models.ForeignKey('Site_User')
	amount = models.IntegerField()
	date = models.DateTimeField(auto_now_add = True)
