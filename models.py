from django.db import models
from django.contrib.auth.models import User

class User(User):
	sites = models.CommaSeparatedIntegerField(max_length = 100)